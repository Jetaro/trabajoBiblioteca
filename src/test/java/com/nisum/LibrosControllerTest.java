package com.nisum;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.any;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import com.nisum.controller.LibrosController;
import com.nisum.model.Libro;
import com.nisum.service.LibrosService;


@RunWith(MockitoJUnitRunner.class)
public class LibrosControllerTest {

	@Mock
	private LibrosService libroService;
	
	@InjectMocks
	private LibrosController libroController;
	
	Libro miLibro;
	Libro miLibro2;
	Libro miLibro3;	
	
	@Before
	public void setUp() {
		
		miLibro = new Libro();
		miLibro.setId(1L);
		miLibro.setTitulo("El Silmarillion");
		miLibro.setAutor("J.R.R. Tolkien");
		
		miLibro2 = new Libro();
		miLibro2.setId(2L);
		miLibro2.setTitulo("Jugando a ser famoso");
		miLibro2.setAutor("Coolife");
		
		miLibro3 = new Libro();
		miLibro3.setId(3L);
		miLibro3.setTitulo("Divergente");
		miLibro3.setAutor("Veronica Roth");	
		
		RestAssuredMockMvc.standaloneSetup(libroController);
	}
	
	@Test
	public void crearLibro() {
		
		Libro otroLibro= new Libro();
		otroLibro.setId(5L);
		otroLibro.setTitulo("Titulo de prueba");
		otroLibro.setAutor("Jorge Thompson");			
		
		Mockito.when(libroService.crearLibro(Matchers.any(Libro.class))).thenReturn(otroLibro);
		
		given().
			contentType("application/json").
			body(otroLibro).
		when().
			post("/libro/crearLibro").
		then().
			body("id", is(5)).
			body("titulo", equalTo(otroLibro.getTitulo())).
			body("autor", equalTo(otroLibro.getAutor())).
			statusCode(201);
	}
	
	
	@Test
	public void obtenerUnLibroPorId(){
		
		Mockito.when(libroService.obtenerLibro(anyLong())).thenReturn(miLibro);
		
		given().
			contentType("application/json").
		when().
			get("/libro/obtenerLibro/{idLibro}",1).
		then().
			body("id", is(1)).
			statusCode(200);
	}
	
	@Test
	public void obtenerUnLibroPorTitulo(){
		
		Mockito.when(libroService.obtenerLibro(anyString())).thenReturn(miLibro2);
		
		given().
			contentType("application/json").
		when().
			get("/libro/obtenerLibroPorTitulo/{tituloLibro}","Jugando a ser famoso").
		then().
			body("id", is(2)).
			body("titulo", equalTo(miLibro2.getTitulo())).
			body("autor", equalTo(miLibro2.getAutor())).
			statusCode(200);
	}
	
	@Test
	public void obtenerTodosLosLibros(){
				
		ArrayList<Libro> misLibros= new ArrayList<Libro>();
		misLibros.add(miLibro);
		misLibros.add(miLibro2);
		misLibros.add(miLibro3);
		
		Mockito.when(libroService.obtenerTodos()).thenReturn(misLibros);
		
		given().
			contentType("application/json").
		when().
			get("/libro/listaLibros").
		then().
			body("id[0]", is(1)).
			body("titulo[0]",equalTo("El Silmarillion")).
			body("autor[0]",equalTo("J.R.R. Tolkien")).
			body("id[1]", is(2)).
			body("titulo[1]",equalTo("Jugando a ser famoso")).
			body("autor[1]",equalTo("Coolife")).
			body("id[2]", is(3)).
			body("titulo[2]",equalTo("Divergente")).
			body("autor[2]",equalTo("Veronica Roth")).
			statusCode(200);
	}
}
