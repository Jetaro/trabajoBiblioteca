package com.nisum.controller;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.ArrayList;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.nisum.model.Libro;
import com.nisum.service.LibrosService;


@RequestMapping("/libro")
@RestController
public class LibrosController {
	
	@Autowired
	private LibrosService libroService;
	
	
	@RequestMapping (value = "/crearLibro", method = POST)
	@ResponseBody
	public ResponseEntity<Libro>CrearLibro(@RequestBody Libro libro){

		libroService.crearLibro(libro);
		return new ResponseEntity<Libro>(libro, HttpStatus.CREATED);
	}
	
	@RequestMapping (value = "/obtenerLibro/{idLibro}", method = GET)
	@ResponseBody
	public ResponseEntity<Libro>obtenerLibroPorId(@PathVariable("idLibro") long idLibro){		
		return new ResponseEntity<Libro>(libroService.obtenerLibro(idLibro), HttpStatus.OK);
	}

	@RequestMapping (value = "/obtenerLibroPorTitulo/{tituloLibro}", method = GET)
	@ResponseBody
	public ResponseEntity<Libro>obtenerLibroPorTitulo(@PathVariable("tituloLibro") String tituloLibro){		
		return new ResponseEntity<Libro>(libroService.obtenerLibro(tituloLibro), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/listaLibros", method = GET)
	@ResponseBody
	public ResponseEntity<ArrayList<Libro>> obtenerTodosLosLibros() {
		return new ResponseEntity<ArrayList<Libro>>(libroService.obtenerTodos(), HttpStatus.OK);
	}

}
